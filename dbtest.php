<?php

$pdo = new \PDO('mysql:dbname=logger;host=localhost;charset=utf8', 'root', 'root');

// TODO: get rid of axios and use Vue this.$http plugin so we can use logical approach
$data = json_decode(file_get_contents('php://input'), true);

if (isset($data['tasks'])) {
    $tasks = $data['tasks'];

    // TODO: need to figure out storing the multiple ranges for [from, to] so they are recoverable

    foreach ($tasks as $task) {
        // TODO: update if existing
        $stmt = $pdo->prepare('
            INSERT INTO `logs` SET
            `title` = ?,
            `details` = ?,
            `hours` = ?,
            `date` = NOW()
        ');
        $stmt->execute([
            $task['title'],
            implode(PHP_EOL, $task['details']),
            $task['hours'],
        ]);
    }

    echo json_encode([
        'success' => true,
        'message' => 'Tasks are saved',
    ]);
} else {
    echo json_encode([
        'success' => false,
        'message' => 'Request data is empty',
    ]);
}
