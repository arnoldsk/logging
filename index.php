<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Logging</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/solar/bootstrap.min.css"> -->
</head>
<body style="padding-top: 60px;">
    <div id="app">
        <div class="container" v-show="currentState === state.FORM">
            <div class="row">
                <div class="col-md-6">
                    <!-- TODO: this is a component -->
                    <form autocomplete="off" @submit.prevent="submitForm">
                        <div class="form-group">
                            <label for="jira-browse-url">Jira Browse URL</label>
                            <input
                                type="url"
                                class="form-control"
                                id="jira-browse-url"
                                placeholder="https://xxxx.atlassian.net/browse/"
                                @change="updateJiraUrl"
                            >
                        </div>
                        <div class="form-group" v-for="(task, key) in tasks" :key="key">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group">
                                        <input
                                            type="text"
                                            class="form-control"
                                            placeholder="ABC-123 - Blah blah"
                                            v-model="task.title"
                                            @change="updateTasks(task, key)"
                                        >
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input
                                                    type="text"
                                                    class="form-control"
                                                    placeholder="From"
                                                    :pattern="timePattern"
                                                    v-model="task.from"
                                                    @change="updateTasks(task, key)"
                                                >
                                            </div>
                                            <div class="col-md-6">
                                                <input
                                                    type="text"
                                                    class="form-control"
                                                    placeholder="To"
                                                    :pattern="timePattern"
                                                    v-model="task.to"
                                                    @change="updateTasks(task, key)"
                                                >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea
                                            class="form-control"
                                            rows="10"
                                            placeholder="- Task detail"
                                            v-model="task.details"
                                            @input="updateTasks(task, key)"
                                        ></textarea>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="button" class="btn btn-danger btn-sm" @click="removeTask(key)">Remove</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-primary" @click="addTask">Add a task</button>
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <!-- TODO: there is no spacing between cards -->
                    <div class="card" v-for="(task, title) in groupedTasks">
                        <div class="card-header">{{ title }}</div>
                        <div class="card-body">
                            <ul>
                                <li v-for="detail in task.details">{{ detail }}</li>
                            </ul>
                        </div>
                        <div class="card-footer">{{ task.hours }}</div>
                    </div>

                    <!-- Figure this out -->
                    <hr>
                    Thu 28 Mar - {{ totalHours }} hour(s)
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- Axios -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>

    <!-- Vue -->
    <script src="//cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="js/main.js"></script>
</body>
</html>
