
const app = new Vue({
    el: '#app',

    data: {
        currentState: 0,
        state: {
            FORM: 0,
        },
        jiraUrl: '',
        tasks: [],
        groupedTasks: [],
        totalHours: 0,
        timePattern: '([0-9]{1,2}:[0-9]{2})',
    },

    methods: {
        updateJiraUrl(e) {
            this.jiraUrl = e.target.value;
        },

        async submitForm() {
            const resp = await axios({
                method: 'post',
                url: 'dbtest.php',
                data: {
                    tasks: this.groupedTasks,
                },
            });

            console.log(resp.data);
        },

        addTask() {
            this.tasks.push({
                title: '',
                from: '',
                to: '',
                details: '',
            });
        },

        formatTaskTitle(title) {
            let result = title;
            const regExp = /([A-Za-z]+-[0-9]+)(.*)/;

            if (regExp.test(title)) {
                const matches = regExp.exec(title);
                const number = matches[1].toUpperCase();
                let label = matches[2];

                result = number;

                // Add the dash if missing
                if (label.length && label.trim().indexOf('-') !== 0) {
                    result += ` - ${label.trim()}`;
                } else {
                    result += label;
                }
            }

            return result;
        },

        formatTaskTime(time) {
            const timeRegExp = new RegExp(this.timePattern);

            time = time.trim().match(timeRegExp);

            return time ? time[1] : '';
        },

        formatTaskDetails(details) {
            return details.split('\n')
                .map((detail) => (detail.indexOf('-') !== 0) ? `- ${detail}` : detail)
                .join('\n');
        },

        updateTasks(task, key) {
            task.title = this.formatTaskTitle(task.title);
            task.from = this.formatTaskTime(task.from);
            task.to = this.formatTaskTime(task.to);
            task.details = this.formatTaskDetails(task.details);

            this.tasks[key] = task;
            this.groupTasks();
        },

        groupTasks() {
            const groupedTasks = {};

            for (const task of this.tasks) {
                const group = groupedTasks[task.title] || {
                    title: '',
                    details: [],
                    hours: 0,
                    times: [],
                };

                group.title = task.title;
                group.details = [...group.details, ...task.details.split('\n')];
                group.hours += this.rangeToHours(task.from, task.to);
                group.times.push({
                    from: task.from,
                    to: task.to,
                });

                groupedTasks[task.title] = group;
            }

            // Update total hours
            // TODO: possible can move to watcher
            this.totalHours = 0;
            for (const task of Object.values(groupedTasks)) {
                this.totalHours += task.hours;
            }

            this.groupedTasks = groupedTasks;
        },

        rangeToHours(from, to) {
            const dateFrom = new Date(`01-01-1970 ${from}`);
            const dateTo = new Date(`01-01-1970 ${to}`);
            const hours = (dateTo - dateFrom) / 1000 / 60 / 60;

            return Math.floor(hours * 100) / 100;
        },

        removeTask(key) {
            if (confirm('Are you sure?')) {
                this.tasks.splice(key, 1);
            }
        },
    },
});
